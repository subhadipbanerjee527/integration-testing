terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/integration-testing/terraform/state/s3creation"
    lock_address   = "https://gitlab.com/api/v4/projects/integration-testing/terraform/state/s3creation/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/integration-testing/terraform/state/s3creation/lock"
    username       = "gitlab"
    # password      = not in configuration
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}
